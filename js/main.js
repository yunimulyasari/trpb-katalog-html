var w = window,
d = document,
e = d.documentElement,
g = d.getElementsByTagName('body')[0],
widthviewport = w.innerWidth || e.clientWidth || g.clientWidth,
heightviewport = w.innerHeight || e.clientHeight || g.clientHeight;

// handle 100vh in safari ios
(function(){
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
})();

// Dropdown menu on header
(function(){

    $(function(){
      var status = new DropDown( $('.dropdown') );
    });

    function DropDown(el){
        this.user = el;
        this.name = this.user.find('.dropdown-selected');
        this.menu = this.user.find('.dropdown-menu');
        this.initEvents();
        this.closeEvents();
    }

    DropDown.prototype = {
        initEvents : function(){
            var obj = this;

            obj.name.on('click', function(event){
                event.stopPropagation();
                
                var ths = $(this).parent();
                if(ths.hasClass('active')){
                    $('.dropdown').removeClass('active');
                }
                else{
                    $('.dropdown').removeClass('active');
                    $(this).parent().toggleClass('active');
                }
                return false;
            });            
            
        },
        closeEvents : function(){
            var ths = this;
            $(document).on('click', function(){
                $('.dropdown').removeClass('active');
            });

            ths.menu.on('click', function(e){
                e.stopPropagation();
            });
        }
    }

})();

// Collapsible
(function(){
    var buttonCollapse = $('.collapsible-button');
    var targetCollapse = $(buttonCollapse).attr('data-target');

    buttonCollapse.on('click', function(e){
        e.preventDefault();

        var colId = $(this).attr('data-target');

        $(colId).slideToggle(300);
        $(this).toggleClass('collapse');
    });

    $(document).ready(function(){
        if(buttonCollapse.hasClass('collapse')){
            targetCollapse.slideToggle(300);
        }
    });    
})();


// show hide custom radio las added filter
(function(){
    $("input[name$='last-added']").on('change', function(){
        var inputValue = $(this).val();

        if(inputValue == "customLastDate"){
            $("#customLastDate").fadeIn(300);
        }
        else{
            $("#customLastDate").hide();
        }
    });
})();

// show hide search result on type search bar
(function(){
    $(".search-bar-input").focusin(function() {
        var target = $(this).attr('data-target');
        $(target).show();
    }).focusout(function () {
        var target = $(this).attr('data-target');
        $(target).hide();
    });


    $(".search-popup-mobile-button").on('click', function(e){
        e.preventDefault();

        var target = $(this).attr('data-target');

        $(target).toggleClass('show');
    })

    $(".search-popup-back").on('click', function(e){
        e.preventDefault();
        $(this).parents(".search-popup-mobile").removeClass('show');
    });
})();

// show filter on mobile
(function(){
    $(".filter-mobile-button").on('click', function(e){
        e.preventDefault();
        var target = $(this).attr('data-target');

        $(target).toggleClass('show');
    });

    $(".filter-mobile-close").on('click', function(e){
        e.preventDefault();
        $(this).parents(".filter-mobile").removeClass('show');
    });
})();

// show pass inpur login
(function(){
    var input = $(".show-pass-input");

    $('.btn-show-pass').click(function(e){
        input.toggleClass('showed');
        
        if(input.hasClass('showed')){
            input.attr('type','text');
        }else{
            input.attr('type','password');
        }
    });
})();

// on scroll see detail item
(function(){
    var drawerBody = $(".drawer-body");

    drawerBody.scroll(function (event) {
        var scroll = $(this).scrollTop();
        var fileSource = $(this).parents(".drawer-detail-item").find(".file-source");
        var fileName = $(this).parents(".drawer-detail-item").find(".file-name");

        if(scroll >= 60){
            fileSource.hide();
            fileName.show();
        }
        if(scroll < 60){
            fileSource.show();
            fileName.hide();
        }
    });


    var drawerButtonOpen = $(".drawer-button");
    var drawerButtonClose = $(".drawer-close");

    drawerButtonOpen.on('click', function(e){
        var target = $(this).data("drawer-target");

        $('.timeline-item').removeClass('active');
        
        $(target).addClass('active');
        $(this).parents('.timeline-item').addClass('active');
    });

    drawerButtonClose.on('click', function(){
        $(this).parents(".drawer-detail-item").removeClass('active');
        $('.timeline-item').removeClass('active');
    });

})();